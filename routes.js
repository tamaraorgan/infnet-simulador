const express = require('express')

const indexController = require('./controllers/indexController')
const registerController = require('./controllers/registerController')
const aboutController = require('./controllers/aboutController')

const routes = express.Router()

routes.get('/', indexController.index)

routes.get('/register', registerController.index)
routes.post('/register', registerController.create)

routes.get('/about', aboutController.index)

module.exports = routes