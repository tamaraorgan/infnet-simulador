// teste
const express = require('express')
const path = require('path')
const cors = require('cors')
const bodyParser = require('body-parser')
const routes = require('./routes')

const app = express()

app.use(cors())
app.set('view engine', 'ejs')
app.use(express.static(path.join(__dirname, 'public')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use(routes)

const port = process.env.PORT || 3333

app.listen(port, () => {
  console.log('ᕦ(ツ)ᕤ Server starded on port 3333!')
})
