# CADASTRO PARA SOLICITAR EMPRÉSTIMOS
Projeto criado com estrutura monolítica, onde o back-end e o front-end foram feitos juntos para fins educativos.

## Primeiro passos
No diretório do projeto execute yarn para instalar as dependencias.
## Scripts Disponiveis
No diretório do projeto, você pode executar:

### `yarn dev`
Executa o aplicativo no modo de desenvolvimento. \
Abra [http: // localhost: 3000] (http: // localhost: 3000) para visualizá-lo no navegador.
A página será recarregada se você fizer edições. \
Você também verá quaisquer erros de lint no console.