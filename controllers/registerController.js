const fs = require('fs')
const ejs = require('ejs')
const htmlPDF = require('html-pdf-node')

const selectTypeModel = require('../models/SelectType')
const selectValuePortionModel = require('../models/SelectValuePortion')

module.exports = {
  index(request, response) {
    const responseModelSelectType = selectTypeModel.getAllSelectType()
    const selectTypeItem = responseModelSelectType.map(item => {
      return {
        value: item.id,
        label: item.descrition
      }
    })

    const responseModelValuePortion = selectValuePortionModel.getAllSelectValuePortion() //ele pega a função getAllSelect dentro do Models e executa, armazenando na const a resposta da função
    const selectValuePortionItem = responseModelValuePortion.map(item => {
      return {
        value: item.id,
        label: item.descrition
      }
    }) // ele configura como será o retorno da resposta
    const getViewModels = {
      selectTypeItem,
      selectValuePortionItem
    }

    response.render('register', getViewModels)
  },

  create(request, response) {
    const body = request.body
    console.log(body)
    const {
      selectOptionType,
      inputValuePortion,
      value,
      name,
      email,
      phone,
      cep
    } = request.body // pega informações do body

    const renderInputSelectType = selectTypeModel.getByIdSelectType(
      selectOptionType
    )
    const renderInputValuePortion = selectValuePortionModel.getByIdSelectValuePortion(
      inputValuePortion
    )

    const dadosForm = {
      selectOptionType: renderInputSelectType.descrition,
      inputValuePortion: renderInputValuePortion.descrition,
      value,
      name,
      email,
      phone,
      cep
    } // coloca as informações vindas do body e armazena em uma variável

    var htmlText = fs.readFileSync('./views/dadosPDF.ejs', 'utf8') //ele pega o arquivo e transforma legivel

    var htmlRenderzided = ejs.render(htmlText, dadosForm) //pega o html bruto e transforma em pdf

    let file = { content: htmlRenderzided }
    let options = { format: 'A4' }

    htmlPDF.generatePdf(file, options).then(pdfBuffer => {
      response.contentType('application/pdf')
      response.send(pdfBuffer)
    }) //gera o pdf e retorna ele no browser (.then é usando quando já executa algo assim que a promessa for cumprida)
  }
}
