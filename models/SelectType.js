const selectTypeData = [
  {
    id: '1',
    descrition: 'Imóveis'
  },
  {
    id: '2',
    descrition: 'Automóveis'
  },
  {
    id: '3',
    descrition: 'Motos'
  },
  {
    id: '4',
    descrition: 'Caminhões'
  },
  {
    id: '5',
    descrition: 'Serviços'
  }
]

const getByIdSelectType = id => {
  const resposta = selectTypeData.find(item => {
    return item.id === id
  })
  return resposta
}

const getAllSelectType = () => {
  return selectTypeData
}

module.exports = {
  getByIdSelectType,
  getAllSelectType
}
