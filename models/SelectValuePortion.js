const selectValuePortionData = [
  {
    id: '1',
    descrition: 'Valor'
  },
  {
    id: '2',
    descrition: 'Parcela'
  }
]

const getByIdSelectValuePortion = id => {
  const resposta = selectValuePortionData.find(item => {
    return item.id === id
  })
  return resposta
}

const getAllSelectValuePortion = () => {
  return selectValuePortionData
}

module.exports = {
  getByIdSelectValuePortion,
  getAllSelectValuePortion
}
