/*********************** MENU BAR **************************************/
window.onload = function () {
  document.querySelector('.menuMobile').addEventListener('click', function () {
    if (document.querySelector('#menu ul').style.display == 'flex') {
      document.querySelector('#menu ul').style.display = 'none'
    } else {
      document.querySelector('#menu ul').style.display = 'flex'
    }
  })
}

/******************************* WRITER TITLE *********************************/
function typeWriter(e) {
  const textoArray = e.innerHTML.split('')
  e.innerHTML = ''
  textoArray.forEach((letra, i) => {
    setTimeout(() => (e.innerHTML += letra), 75 * i)
  })
}
const titulo = document.querySelector('#titleH2')
typeWriter(titulo)

/************************** SHOW VALUE INPUT RANGE ****************************/
function showRangeValue(value) {
  document.getElementById('valueRangeResult').innerHTML = "R$" + value + ",00"
}
